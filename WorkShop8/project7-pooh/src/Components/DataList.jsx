import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import GitHubIcon from "@mui/icons-material/GitHub";
import spotify from '../assets/spotify.png';
import rent from '../assets/rent.png';
import './DataList.css';

export default function DataList() {
  return (
    <div>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          paddingLeft: "2%",
          paddingRight: "2%",
          alignItems: "baseline",
          marginBottom:"2%",
          minWidth:"300px",
        }}
      >
        <Typography sx={{ fontSize: "3.5vh", fontWeight: "bold" }}>
          Transactions
        </Typography>
        
        <Button sentenceCase variant="see">
        <Typography sx={{ fontSize: "2.5vh", fontWeight: "600", color:"gray" }}>
            
          See all
        </Typography>
        </Button>
        
      </Box>

      <Box
        sx={{
          borderRadius: "2rem",
          backgroundColor: "#F5F5FB",
          //   marginLeft: "1%",
          //   paddingLeft: "1%",
          //   marginRight: "1%",
          //   paddingRight: "1%",
          padding: "2%",
          marginBottom: "2%",
          display: "flex",
          alignItems: "center",
          minWidth:"300px",
        }}
      >
        <Box
          sx={{
            backgroundColor: "white",
            width: "15%",
            alignItems: "center",
            borderRadius: "2rem",
            padding: "4%",
          }}
        >
          <GitHubIcon sx={{ width: "90%", height: "90%" }} />
        </Box>
        <Box
          sx={{ marginLeft: "0.8rem", backgroundColor: "#F5F5FB", width: "48%" }}
        >
          <Typography
            sx={{ fontSize: "3vh", fontWeight: "bold" }}
          >
            Github
          </Typography>
          <Typography
            sx={{ fontSize: "2vh", fontWeight: "light" }}
          >
            Sep 12 at 1:24 PM
          </Typography>
        </Box>

        <Typography
          sx={{ fontSize: "1.2rem", fontWeight: "bold",marginLeft:"5%"}}
        >
          -$29.99
        </Typography>
      </Box>




      <Box
        sx={{
          borderRadius: "2rem",
          backgroundColor: "#F5F5FB",
          //   marginLeft: "1%",
          //   paddingLeft: "1%",
          //   marginRight: "1%",
          //   paddingRight: "1%",
          padding: "2%",
          marginBottom: "2%",
          display: "flex",
          alignItems: "center",
          minWidth:"300px",
        }}
      >
        <Box
          sx={{
            backgroundColor: "white",
            width: "15%",
            alignItems: "center",
            borderRadius: "2rem",
            padding: "4%",
          }}
        >
          {/* <GitHubIcon sx={{ width: "90%", height: "90%" }} /> */}
        <img src={rent} alt="rent" style={{width: "90%", height: "90%"}}/>
        </Box>
        <Box
          sx={{ marginLeft: "0.8rem", backgroundColor: "#F5F5FB", width: "48%" }}
        >
          <Typography
            sx={{ fontSize: "3vh", fontWeight: "bold" }}
          >
            Rent
          </Typography>
          <Typography
            sx={{ fontSize: "2vh", fontWeight: "light" }}
          >
            Sep 4 at 5:12 PM
          </Typography>
        </Box>

        <Typography
          sx={{ fontSize: "1.2rem", fontWeight: "bold",marginLeft:"5%"}}
        >
          -$940.00
        </Typography>
      </Box>




      <Box
        sx={{
          borderRadius: "2rem",
          backgroundColor: "#F5F5FB",
          //   marginLeft: "1%",
          //   paddingLeft: "1%",
          //   marginRight: "1%",
          //   paddingRight: "1%",
          padding: "2%",
          marginBottom: "2%",
          display: "flex",
          alignItems: "center",
          minWidth:"300px",
        }}
      >
        <Box
          sx={{
            backgroundColor: "white",
            width: "15%",
            alignItems: "center",
            borderRadius: "2rem",
            padding: "4%",
          }}
        >
          {/* <GitHubIcon sx={{ width: "90%", height: "90%" }} /> */}
          <img src={spotify} alt="spotify" style={{width: "90%", height: "90%"}}/>
        </Box>
        <Box
          sx={{ marginLeft: "0.8rem", backgroundColor: "#F5F5FB", width: "48%" }}
        >
          <Typography
            sx={{ fontSize: "3vh", fontWeight: "bold" }}
          >
            Spotify
          </Typography>
          <Typography
            sx={{ fontSize: "2vh", fontWeight: "light" }}
          >
            Oct 28 at 9:05 PM
          </Typography>
        </Box>

        <Typography
          sx={{ fontSize: "1.2rem", fontWeight: "bold",marginLeft:"5%"}}
        >
          -$9.99
        </Typography>
        
      </Box>

      
    </div>
  );
}
