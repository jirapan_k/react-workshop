import { Line } from "react-chartjs-2";
import {
  Chart as ChartJS,
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  Legend,
} from "chart.js";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import "./Graph.css";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

ChartJS.register(LineElement, CategoryScale, LinearScale, PointElement, Legend);

export default function GraphData() {
  const data = {
    labels: ["1M", "2M", "3M", "4M", "5M", "6M", "7M", "8M", "9M", "10M"],

    datasets: [
      {
        label: "",
        data: [0, 3, 2, 4, 5, 10, 5, 2, 4, 6],
        backgroundColor: "white",
        borderColor: "white",
        tension: 0.4,
        fill: true,
      },
    ],
  };

  const options = {
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      x: {
        ticks: {
          display: true,
        },
        grid: {
          drawBorder: false,
          display: false,
        },
      },
      y: {
        ticks: {
          display: true,
          beginAtZero: true,
        },
        grid: {
          drawBorder: false,
          display: false,
        },
      },
    },
  };

  return (
    <div className="Graph">
      <div
        style={{
          width: "auto",
          height: "auto",
        }}
      >
        <Box
          sx={{
            paddingLeft: "2%",
            paddingRight: "2%",
            alignItems: "baseline",
            marginBottom: "2%",
          }}
        >
          <AccountBalanceIcon
          color="inherit" style={{ color: "black" }}
            sx={{
              marginTop: "-15%",
              width: "15%",
              height: "15%",
              
              backgroundImage: "linear-gradient(to right, #A6FBBD, #DCF69B)",
              borderRadius: "10rem",
              padding: "4%",
            }}
          />
          <Typography sx={{ fontSize: "3.5vh", fontWeight: "bold" }}>
            Checking
          </Typography>

          <Typography sx={{ fontSize: "2.5vh", fontWeight: "bold" }}>
            •••• 5678
          </Typography>
          <Typography sx={{ fontSize: "2.5vh", fontWeight: "lighter" }}>
            Bank of America
          </Typography>
        </Box>
        <Line data={data} options={options}></Line>
      </div>
    </div>
  );
}
