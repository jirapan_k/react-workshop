import { Box, IconButton, Typography } from "@mui/material";
import Profile from "../img/profile.jfif";
import "./Header.css";
import NotificationsIcon from "@mui/icons-material/Notifications";
export default function Header() {
  return (
    <>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginTop: "10px",
          minWidth:"300px"
        }}
      >
        <Box sx={{m:1}}>
          <img src={Profile} alt="Profile" className="profile" />
        </Box>
        <Box>
          <Typography
            sx={{ fontSize: "20px", fontWeight: "bold", marginTop: "10px" }}
          >
            Wed, Sep 15
          </Typography>
        </Box>
        <Box sx={{m:1}}>
          <IconButton>
            <NotificationsIcon sx={{ color: "gray" }} />
          </IconButton>
        </Box>
      </Box>
    </>
  );
}
