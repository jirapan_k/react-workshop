import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "@mui/material/Link";
import { Avatar } from "@mui/material";
import { ButtonGroup } from "@mui/material";

export default function UserCard() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch("https://www.melivecode.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const UserUpdate = (id) => {
    window.location = "/update/" + id;
  };

  const UserDelete = (id) =>{
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
      id : id,
    });

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.melivecode.com/api/users/delete", requestOptions)
    .then(response => response.json ())
    .then(result => {
      alert(result["message"])
      if(result['status'] === 'ok'){
        UserGet();
      }
    })
      .catch((error) => console.log("error", error));
  }

  

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%" sx={{ p: 2 }}>
        <Box className="box">
          <Link href="create" sx={{ m: 2 }}>
            <Button variant="contained">Create</Button>
          </Link>

          <Grid
            container
            spacing={4}
            direction="row"
            justifyContent="center"
            alignItems="center"
        
            
          >

            
            {items.map((row) => (
              <Grid item xs={12} md={4} key={row.id}>
                <Card >
                  <CardContent>
                    <Box>
                      <Avatar alt={row.username} src={row.avatar} />
                    </Box>

                    <Typography variant="body2" color="text.secondary">
                      Name : {row.fname}
                    </Typography>
                    <Typography variant="body2" color="text.secondary"> 
                      Last name : {row.lname}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      username : {row.username}
                    </Typography>
                  </CardContent>
                  <CardActions>
                  <ButtonGroup variant="outlined" aria-label="outlined button group">
                    <Button onClick={()=>UserUpdate(row.id)} size="small">
                      Edit
                    </Button>
                    <Button onClick={()=>UserDelete(row.id)} size="small">
                      Delete
                    </Button>
                    </ButtonGroup>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Box>
      </Container>
    </React.Fragment>
  );
}
