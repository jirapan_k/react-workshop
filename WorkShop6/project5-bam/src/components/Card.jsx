import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import { useState, useEffect } from "react";
import TableContainer from "@mui/material/TableContainer";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import "./Card.css";
import "./Navbar.css";

export default function MediaCard() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch("https://www.melivecode.com/api/users")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const UserUpdate = (id) => {
    window.location = "/update/" + id;
  };

  const UserDelete = (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      id: id,
    });

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch("https://www.melivecode.com/api/users/delete", requestOptions)
      .then((response) => response.json())
      .then((result) => {
        alert(result["message"]);
        if (result["status"] === "ok") {
          UserGet();
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <>
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg" sx={{ p: 2 }}>
          <Paper sx={{ p: 2 }}>
            <Box display="flex">
              <Box sx={{ flexGrow: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  Users
                </Typography>
              </Box>
              <Box>
                <Link href="create">
                  <Button
                    className="button"
                    sx={{ backgroundColor: "#19a7ce", color: "white" }}
                  >
                    Create
                  </Button>
                </Link>
              </Box>
            </Box>
            <TableContainer component={Paper} sx={{ m: 0 }}>
              <Box
                sx={{
                  display: "grid",
                  gridTemplateColumns: "repeat(auto-fill, minmax(320px, 1fr))",
                  gap: "70px",
                  backgroundColor:"e1e1e1"
                }}
              >
                {items.map((row) => (
                  <Card sx={{ maxWidth: 345 }} className="card">
                    <CardMedia sx={{ height: 140 }} className="avatar">
                      <Avatar
                        sx={{
                          height: 180,
                          width: 180,
                          margin: 10,
                          display: "block",
                        }}
                        alt={row.username}
                        src={row.avatar}
                      />
                    </CardMedia>
                    <CardContent key={row.id} className="card-content">
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="div"
                        className="id"
                        sx={{fontWeight:"blod", color:"#19a7ce",backgroundColor:"#e5e5e5",borderRadius:"5px"}}
                      >
                        ID:{row.id}
                      </Typography>
                      <Typography variant="body2" color="text.secondary">
                        First Name: {row.fname} <br />
                        Last Name: {row.lname} <br />
                        Username: {row.username} <br />
                      </Typography>
                    </CardContent>
                    <CardActions className="btn-group">
                      <Button
                        className="button"
                        sx={{backgroundColor:"#19a7ce", color:"white"}}
                        onClick={() => UserUpdate(row.id)}
                      >
                        Edit
                      </Button>
                      <Button
                        className="button"
                        sx={{backgroundColor:"#19a7ce", color:"white"}}
                        onClick={() => UserDelete(row.id)}
                      >
                        Delete
                      </Button>
                    </CardActions>
                  </Card>
                ))}
              </Box>
            </TableContainer>
          </Paper>
        </Container>
      </React.Fragment>
    </>
  );
}
