import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Link from "@mui/material/Link";
import { useState, useEffect } from "react";
import { ButtonGroup } from "@mui/material";
import "./Table.css";

const urlApiAll =
  "https://735c-110-170-188-98.ngrok-free.app/customer?fbclid=IwAR36jCssLA1whZ5FHRXs0nf1aG3rVsp28aVbW0iLs5NAtL1phfOUTrxxdXY";
const urlApi = "https://735c-110-170-188-98.ngrok-free.app/customer";

export default function SimpleContainer() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch(urlApiAll)
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const UserEdit = (id) => {
    window.location = "/edit/" + id;
  };
  const UserDel = (id) => {
    console.log(id);
    console.log(urlApi + "/" + id, requestOptions);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "DELETE",
      redirect: "follow",
    };

    fetch(urlApi + "/" + id, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        alert(result);
        if (result === "Removed Customer Complete") {
          UserGet();
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" sx={{ p: 2 }}>
        <Paper sx={{ p: 2 }}>
          <Box display="flex">
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="h6" gutterBottom>
                User
              </Typography>
            </Box>
            <Box>
              <Link href="create">
                <Button variant="contained">Create</Button>
              </Link>
            </Box>
          </Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead className="head_table">
                <TableRow>
                  <TableCell align="center">ID</TableCell>
                  <TableCell align="left">Name</TableCell>
                  <TableCell align="center">Phone Number</TableCell>
                  <TableCell align="center">Birth Date</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {items.map((row) => (
                  <TableRow
                    key={row.Customer_name}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" align="center">
                      {row.Customer_id}
                    </TableCell>
                    <TableCell align="left">{row.Customer_name}</TableCell>
                    <TableCell align="center">{row.Phone_number}</TableCell>
                    <TableCell align="center">{row.Birth_date}</TableCell>
                    <TableCell align="center">
                      <ButtonGroup
                        variant="outlined"
                        aria-label="outlined button group"
                      >
                        <Button onClick={() => UserEdit(row.Customer_id)}>
                          Edit
                        </Button>
                        <Button onClick={() => UserDel(row.Customer_id)}>
                          Del
                        </Button>
                      </ButtonGroup>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Container>
    </React.Fragment>
  );
}
