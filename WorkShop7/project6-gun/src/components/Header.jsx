import { Box } from "@mui/material";
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import Profile from "../imgs/profile.jfif";
import WavingHandIcon from '@mui/icons-material/WavingHand';
import "./Header.css";
export default function Header(){
    return(
        <>
        <Box sx={{ height: "10vh" ,display:"flex" ,justifyContent:"space-between",}}>
            <Box sx={{marginTop:"10px"}}>
              <Box sx={{ margin: "20px 10px" }}>Hello,</Box>
              <Box sx={{ margin: "-20px 10px",fontWeight:"bold",display:"flex",alignItems:"center"}}>George Chichua&nbsp; < WavingHandIcon sx={{color:"#fe0",fontSize:"20px"}}/></Box>
            </Box>
            <Box sx={{display:"flex", alignItems: 'center',marginTop:"40px"}}>
              <Box sx={{ margin: "20px 10px" }}><NotificationsNoneOutlinedIcon/></Box>
              <Box sx={{ margin: "20px 10px" }} >
                <img src={Profile} alt="profile" />
              </Box>
            </Box>
          </Box>
          </>
    );
}