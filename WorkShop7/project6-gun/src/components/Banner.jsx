import { Box, Button, Typography } from "@mui/material";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import VerifiedOutlinedIcon from "@mui/icons-material/VerifiedOutlined";
import "./Banner.css";
import { green, purple } from "@mui/material/colors";
export default function Banner() {
  return (
      <Box
        className="container-banner"
        sx={{
          color: "#fff",
          alignItems: "center",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <Box sx={{ p: 1 }}>
          <Typography
            sx={{ fontSize: "12px", display: "flex", alignItems: "center" }}
          >
            <VerifiedOutlinedIcon />
            Verify Artist
          </Typography>
          <Typography sx={{ fontSize: "25px", fontWeight: "bold" }}>
            The Weeknd
          </Typography>
          <Typography sx={{ fontSize: "13px" }}>
            97,102,123 mounthly liseners
          </Typography>
        </Box>
        <Box sx={{ p: 1 }}>
          <Button variant="contained" sx={{ bgcolor: "#fff" }}>
            <Typography
              sx={{ fontSize: "12px", fontWeight: "bold", color: "#000" }}
            >
              Follow
            </Typography>
          </Button>
        </Box>
      </Box>
  );
}
{
  /* <Grid
  container
  spacing={2}
  sx={{ alignItems: "center", margin: "0 auto", padding: "5% 0" }}
>
  <Grid item xs={7.5}>
    <Typography
      sx={{ fontSize: "12px", display: "flex", alignItems: "center" }}
    >
      <VerifiedOutlinedIcon />
      Verify Artist
    </Typography>
    <Typography sx={{ fontSize: "25px", fontWeight: "bold" }}>
      The Weeknd
    </Typography>
    <Typography sx={{ fontSize: "13px" }}>
      97,102,123 mounthly liseners
    </Typography>
  </Grid>
  <Grid item xs={4.5}>
    <Button variant="contained" sx={{ bgcolor: "#fff" }}>
      <Typography sx={{ fontSize: "12px", fontWeight: "bold", color: "#000" }}>
        Follow
      </Typography>
    </Button>
  </Grid>
</Grid>; */
}
