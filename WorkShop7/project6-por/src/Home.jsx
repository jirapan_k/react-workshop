import { BsCloudSunFill } from "react-icons/bs";
import * as React from 'react';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import SearchIcon from '@mui/icons-material/Search';
import "./Home.css"
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import Box from '@mui/material/Box';
import { GoSettings } from "react-icons/go";


export default function Home(){
    const Search = styled('div')(({ theme }) => ({
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing(1),
          width: 'auto',
          
        },
      }));
      
      const SearchIconWrapper = styled('div')(({ theme }) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        // display: 'flex',
        textAlign:'left',
        color:'gray',
        alignItems: 'center',
        justifyContent: 'center',
        border:'1px solid gray',
        borderRadius:'5px',
        width:'30vh',
        border:'0',
        paddingTop:'1.5vh',
        backgroundColor:'#e7e7e7'

      }));
      
      const StyledInputBase = styled(InputBase)(({ theme }) => ({
        color: 'inherit',
        '& .MuiInputBase-input': {
          padding: theme.spacing(1, 1, 1, 0),
          // vertical padding + font size from searchIcon
          paddingLeft: `calc(1em + ${theme.spacing(4)})`,
          transition: theme.transitions.create('width'),
          width: '100%',
          [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
              width: '20vh',
            },
          },
        },
      }));

    
    return(
        <div className="containerHome">
        <div className="Header">
            <h2>Hello John , </h2>
            <BsCloudSunFill className="IconSun" />
        </div>
        <div className="select" >
        <Box sx={{ minWidth: 120 }}>
            <FormControl  >
                
                <NativeSelect
                defaultValue={30}
                inputProps={{
                    name: 'delhi',
                    id: 'uncontrolled-native',
                    
                }}
                >
                <option className="selectdetails"  value={10}>New Delhi3</option>
                <option className="selectdetails"  value={20}>New Delhi2</option>
                <option className="selectdetails"  value={30}>New Delhi</option>
                </NativeSelect>
            </FormControl>

        </Box>
        </div>
         <Box className="search">
         <Search sx={{marginBottom:1,paddingTop:0}} className=" Boxsearch">
            <SearchIconWrapper>
              <SearchIcon />
            </SearchIconWrapper>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ 'aria-label': 'search' }}
            />
          </Search>
          <GoSettings className="button-set"/>
         </Box>
        </div>
    )
}
