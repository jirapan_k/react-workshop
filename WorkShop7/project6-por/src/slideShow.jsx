import './slideShow.css'

export default function SlideShow(){
    return(
        <div className='slide'>
            <h3 className='textslide'>How to make your <br />
            subjects interesting</h3>
            <p>Tap to view detail</p>
            <button>Read now</button>
        </div>
    )
}