import "./App.css";
import Header from "./Components/Header";
import AddForm from "./Components/AddForm";
import Item from "./Components/Item";
import { useState, useEffect } from "react";

function App() {
  const [tasks, setTask] = useState(
    JSON.parse(localStorage.getItem("tasks")) || []
  );
  const [title, setTitle] = useState("");
  const [editId, setEditId] = useState(null);
  const [theme, setTheme] = useState("light");

  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
    console.log("บันทึกแล้วน้าาาา");
  }, [tasks]);

  function deleteTask(id) {
    const result = tasks.filter((item) => item.id !== id);
    setTask(result);
    console.log(id);
  }

  function saveTask(e) {
    e.preventDefault();
    if (!title) {
      alert("ไม่มีข้อมูลที่จะทำการเพิ่ม");
    } else if (editId) {
      const updateTask = tasks.map((item) => {
        if (item.id === editId) {
          return { ...item, title: title };
        }
        return item;
      });
      setTask(updateTask);
      setEditId(null);
      console.log(updateTask);
    } else {
      const newTask = {
        id: Math.floor(Math.random() * 1000),
        title: title,
      };
      setTask([...tasks, newTask]);
      setTitle("");
    }
  }

  function editTask(id) {
    setEditId(id);
    const editTaks = tasks.find((item) => item.id === id);
    setTitle(editTaks.title);
    console.log(editTaks);
  }

  return (
    <div className={"App " + theme}>
      <Header theme={theme} setTheme={setTheme} />
      <div className="container">
        <AddForm
          title={title}
          setTitle={setTitle}
          saveTask={saveTask}
          editId={editId}
          theme={theme}
          setTheme={setTheme}
        />
        <section>
          {tasks.map((data) => (
            <Item
              key={data.id}
              data={data}
              deleteTask={deleteTask}
              editTask={editTask}
            />
          ))}
        </section>
      </div>
    </div>
  );
}

export default App;
