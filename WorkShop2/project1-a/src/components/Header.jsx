import React from "react";
import Logo from "../assets/react.svg";
import "../components/Header.css";

const Header = (props) => {
  const { title } = props;
  return (
    <nav>
      <img src={Logo} alt="Logo" className="Logo" />
      <a href="/">{title}</a>
    </nav>
  );
};

export default Header;
