import { Box, Card, CardActionArea, Grid } from "@mui/material";
import "../css/Home.css";
import SettingsIcon from "@mui/icons-material/Settings";
import FilterHdrIcon from "@mui/icons-material/FilterHdr";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";

export default function Home() {
  const [isSmallScreen, setIsSmallScreen] = useState(false);

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-height: 650px)");
    const handleMediaQueryChange = (e) => {
      setIsSmallScreen(e.matches);
    };
    handleMediaQueryChange(mediaQuery);
    mediaQuery.addListener(handleMediaQueryChange);
    return () => {
      mediaQuery.removeListener(handleMediaQueryChange);
    };
  }, []);

  const contentStyle = {
    maxWidth: "85%",
    minWidth: "85%",
    margin: "auto",
    display: "box",
    position: isSmallScreen ? "relative" : "fixed",
    bottom: isSmallScreen ? "5px" : "10px",
    left: isSmallScreen ? "initial" : "50%",
    transform: isSmallScreen ? "initial" : "translateX(-50%)",
  };
  return (
    <>
      <Box id="head">
        Express
        <SettingsIcon id="settingIcon" />
      </Box>
      <hr className="top-hr" />
      <Box id="locetion">
        <p>
          Lao Peng Nguan Tower 1(LPN) Lobby เป็นคอนโดมิเนียม ตั้งอยู่บน 333
          Vibhavadi Rangsit Rd, Khwaeng Chom Phon, Khet Chatuchak, Krung Thep
          Maha Nakhon 10900, Thailand
        </p>
      </Box>

      <Box id="content1">
        <Box id="head-content1">
          เส้นทางประจำ
          <Link>ทั้งหมด</Link>
        </Box>
        <Grid container spacing={2} direction="row" justifyContent="center">
          <Grid item xs={4}>
            <Card variant="outlined" id="card">
              <p className="p1">ส่งให้</p>
              <p className="p2">ทีมบัญชี</p>
              <p className="p3">
                992 ถ.พหลโยธิน ต.คลองหนึ่ง อ.คลองหลวง จ.ปทุมธานี
              </p>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card variant="outlined" id="card">
              <p className="p1">ส่งให้</p>
              <p className="p2">คอนโด life sathorn sierra</p>
              <p className="p3">
                122 ซ. สุขุมวิท 101 แขวง บางจาก เขตพระโขนง กรุงเทพมหานคร
              </p>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card variant="outlined" id="card">
              <p className="p1">ส่งให้</p>
              <p className="p2">TBQ</p>
              <p className="p3">
                122 ซ. สุขุมวิท 101 แขวง บางจาก เขตพระโขนง กรุงเทพมหานคร
              </p>
            </Card>
          </Grid>
        </Grid>
      </Box>

      <Box id="content2">
        <Box id="head-content1">
          รายการล่าสุด
          <Link>ประวัติ</Link>
        </Box>
        <hr />
        <Grid container direction="column" justifyContent="center">
          <Grid item xs>
            <Card variant="outlined" id="card">
              <Box>
                <p className="hp">อยู่ระหว่างดำเดินการ</p>
                <p className="h">กน 7755</p>
              </Box>
              <p className="p2">ส่งสินค้า มหาวิทยาลัยขอนเเก่น</p>
              <p className="p4">จุดผ่าน: 3 จุด ผู้ขับ: ปกรณ์ ชัยณรงค์กุล</p>
            </Card>
          </Grid>
          <Grid item xs>
            <Card variant="outlined" id="card">
              <Box>
                <p className="hs">สำเร็จ</p>
                <p className="h">รน 0984</p>
              </Box>
              <p className="p2">ส่งสินค้า ทีมบัญชี เเสงโสม และTBQ</p>
              <p className="p4">จุดผ่าน: 3 จุด ผู้ขับ: ปกรณ์ ชัยณรงค์กุล</p>
            </Card>
          </Grid>
        </Grid>
      </Box>

      <Box id="content3" style={contentStyle}>
        <p id="head-content3">เรียกคนขับ</p>
        <p id="sub-head-content3">เลือกประเภทรถที่ท่านต้องการ</p>
        <Grid container direction="row" justifyContent="center">
          <Grid item xs={3}>
            <CardActionArea component={Link} to="Detail">
              <Card variant="outlined" id="car">
                <Box id="image">
                  <FilterHdrIcon />
                </Box>
                <p>จักรยานยนต์</p>
              </Card>
            </CardActionArea>
          </Grid>
          <Grid item xs={3}>
            <CardActionArea component={Link} to="Detail">
              <Card variant="outlined" id="car">
                <Box id="image">
                  <FilterHdrIcon />
                </Box>
                <p>รถยนต์</p>
              </Card>
            </CardActionArea>
          </Grid>
          <Grid item xs={3}>
            <CardActionArea component={Link} to="Detail">
              <Card variant="outlined" id="car">
                <Box id="image">
                  <FilterHdrIcon />
                </Box>
                <p>รถตู้</p>
              </Card>
            </CardActionArea>
          </Grid>
          <Grid item xs={3}>
            <CardActionArea component={Link} to="Detail">
              <Card variant="outlined" id="car">
                <Box id="image">
                  <FilterHdrIcon />
                </Box>
                <p>รถกระบะ</p>
              </Card>
            </CardActionArea>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}
