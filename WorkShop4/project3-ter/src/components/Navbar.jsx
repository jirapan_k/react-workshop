import "./Navbar.css"
import { useCart } from "../context/CartContext"
export default function Navbar(){
    const {amount} = useCart()
    return(
        <nav>
              <div className="header">
                <span>Shopping Application</span>
            </div>
            <ul className="link">
                <li>สินค้าในตะกร้า : {amount}</li>
            </ul>
        </nav>
    )
}