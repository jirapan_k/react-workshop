import { createContext, useState } from "react";
import "../src/App.css";
import Navbar from "./components/Navbar";
import Cart from "./components/Cart";

function App() {
  return (
    <div>
      <Navbar />
      <Cart />
    </div>
  );
}

export default App;
