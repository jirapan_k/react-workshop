import React, { useState,useEffect } from "react";
import Header from "./components/Header.jsx"
import AddForm from "./components/AddForm.jsx";
import Item from "./components/Item.jsx";
import "./App.css"; 

function App() {
  const [tasks,setTasks] = useState(JSON.parse(localStorage.getItem("tasks"))||[])
  const [title,setTitle] = useState("")
  const [editId,setEditId] = useState(null);
  const [theme,setTheme] = useState ("light");

  useEffect(()=>{
      localStorage.setItem("Tasks",JSON.stringify(tasks))},[tasks])

 //ฟั่งชั่นแก้ไข Task
  function editTask(id){
        setEditId(id) 
        const editTask = tasks.find((item)=>item.id === id) 
        setTitle(editTask.title)

  }

  function saveTask(e){
      e.preventDefault();
      if(!title){
        alert("กรุณาป้อนข้อมูลด้วยครับ")//ถ้าไม่ได้ใส่ข้อมูล ระบบจะแจ้งเตือน

      }else if(editId){       //อัพเดตข้อมูล
       const updateTask = tasks.map((item)=>{
          if(item.id === editId){
              return {...item,title:title}
          }
          return item;
        })
        setTasks(updateTask)
        setEditId(null)
        setTitle("")
      }else{
        //เพิ่มรายการใหม่
        const newTask={
          id:Math.floor(Math.random()*1000),title:title
        }
        setTasks([...tasks,newTask])
        setTitle("")
      }
  }
  //การ ลบ Task
  function deleteTask(id){
      const result = tasks.filter(item=>item.id !== id)
      setTasks(result);

  }

  return (
    <div className={"App "+theme}>
      
      <Header theme={theme} setTheme={setTheme} />
      <div className="container">
          <AddForm title={title} setTitle={setTitle} saveTask={saveTask} editId={editId} />
          <section>
            {
              tasks.map((data)=>(
                <Item key={data.id} data={data} deleteTask = {deleteTask} editTask={editTask} />
              ))
            }
          </section>
      </div>
    </div>
  );
}

export default App;

