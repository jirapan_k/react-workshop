import notfound from "../images/notfound.svg";

export default function Notfound() {
  return (
    <div className="container">
      <h3 className="title">ไม่พบหน้าเว็บไซต์ (404 Page Not Found)</h3>
      <img src={notfound} alt="not found" />
    </div>
  );
}
