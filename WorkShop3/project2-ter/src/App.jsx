import "./App.css";
import Home from "./components/Home";
import About from "./components/About";
import Blogs from "./components/Blog";
import Navbar from "./components/Navbar";
import Details from "./components/Details";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Notfound from "./components/Notfound";
function App() {
  return (
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />}></Route>
          <Route path="About" element={<About />}></Route>
          <Route path="Blogs" element={<Blogs />}></Route>
          <Route path="*" element={<Notfound />}></Route>
          <Route path="/home" element={<Navigate to="/"/>}></Route>
          <Route path="/info" element={<Navigate to="/About" /> }></Route>
          <Route path="/Blog/:id" element={<Details />}></Route>
        </Routes>
      </BrowserRouter>
  );
}

export default App;
