import about from "../images/Me.png";
import React, { Component } from "react";

class About extends Component {
  render() {
    return (
      <div className="container">
        <h2 className="title">About us</h2>
        <img src={about} alt="about" />
      </div>
    );
  }
}

export default About;
