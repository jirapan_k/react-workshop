import { useState } from "react";
import "./Workshop2.css";

function Workshop2() {
  const [number1, setNumber1] = useState();
  const [number2, setNumber2] = useState();
  const [operator, setOperator] = useState();
  const [cal, setCal] = useState();

  function calculate(num1, ope, num2) {
    if (ope === "+") {
      setCal((parseFloat(num1) + parseFloat(num2)).toFixed(3));
    }

    if (ope === "-") {
      setCal((parseFloat(num1) - parseFloat(num2)).toFixed(3));
    }

    if (ope === "*") {
      setCal((parseFloat(num1) * parseFloat(num2)).toFixed(3));
    }

    if (ope === "/") {
      setCal((parseFloat(num1) / parseFloat(num2)).toFixed(3));
    }
  }

  return (
    <body className="body">
      <div>
        <h1>WorkShop2 React</h1>
        <p style={{fontSize:"30px"}}>วันที่ 27 เมษายน 2566 (Day4)</p>
      </div>
      <div className="content">
        <p className="head">
          พัฒนาฟังก์ชันคำนวณเลขสองจำนวน โดยสามารถเลือก บวก ลบ คูณ หาร ได้
          <br></br>และเมื่อกดปุ่ม Calculate แสดงผลลัพธ์ที่ถูกต้อง ดังตัวอย่าง
        </p>
        <input style={{width:"300px", height:"40px"}}
          type="{float}"
          value={number1}
          onChange={(e) => setNumber1(e.target.value)}
        ></input>
        <select style={{height:"45px"}}
          name="operators"
          onChange={(e) => setOperator(e.target.value)}
          value={operator}
          required
        >
          <option value="+">+</option>
          <option value="-">-</option>
          <option value="*">*</option>
          <option value="/">/</option>
        </select>
        <input style={{width:"300px", height:"40px"}}
          type="{float}"
          value={number2}
          onChange={(e) => setNumber2(e.target.value)}
          required
        ></input>{" "} = {cal}{" "} <br />

        <button onClick={() => calculate(number1, operator, number2)}>Calculate</button>
      </div>
    </body>
  );
}

export default Workshop2;
